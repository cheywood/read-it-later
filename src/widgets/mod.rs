pub mod articles;
mod login;
mod settings;
mod window;

pub use login::LoginWidget;
pub use settings::SettingsWidget;
pub use window::{View, Window};

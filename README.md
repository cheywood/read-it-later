<a href="https://flathub.org/apps/details/com.belmoussaoui.ReadItLater">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>

# Read It Later

<img src="https://gitlab.gnome.org/World/read-it-later/raw/master/data/icons/com.belmoussaoui.ReadItLater.svg" width="128" height="128" />

A Wallabag Client


## Screenshots

<div align="center">
![screenshot](data/resources/screenshots/screenshot1.png)![screenshot](data/resources/screenshots/screenshot2.png)
</div>

## Hack on Read It Later
To build the development version of Read It Later and hack on the code
see the [general guide](https://wiki.gnome.org/Newcomers/BuildProject)
for building GNOME apps with Flatpak and GNOME Builder.

You are expected to follow our [Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct) when participating in project
spaces.
